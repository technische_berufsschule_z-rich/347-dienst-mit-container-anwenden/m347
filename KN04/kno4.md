# Docker Compose Lokal
## Teil A
## Screenshot php Seite 
![alt text](image.png)
## Screenshot db.php
![alt text](image-1.png)
## im gleichen netzerk 
mit docker inspect nachgeschaut. 
```
docker network ls 
docker inspect kn04_ahmadnetz
```
![alt text](image-2.png)
## Was macht Docker Compose?
Docker Compose pulled images wenn nötig und builded sie auch. Danach erstellt es die nötigen Container mit den Configurationen und erstellt auch ein Netzwerk wenn angegeben und vergibt ip Adressen. Es macht also sehr vieles.
## Teil B 
![alt text](image-3.png)
![alt text](image-4.png)
### Erklärung Fehler: 
Es weiss nicht mit welchen Benutzer und Passwort es sich einloggen soll deswegen hat es kein Zugang zu db. wir hatten vorher im environement das Passwort gesetzt und das ist das gleiche wie im db.php
## B) Docker Compose Cloud 
info.php
![alt text](image-7.png)
Hier sieht man info.php hat die ip Adresse 172.10.5.1
db.php
![alt text](image-6.png)
## Vorgang: 
 1. ) erschaffe einen neues keypair oder nutze alte key die du schon hast.
 2. ) generiere aus dem private key der dir aws gibt (.peb) einen Public key mit ssh ssh-keygen -y -f my-aws-key.pem > my-aws-key.pub diesen pub key solltest du in deinem init hinzufügen.
 3. ) Schreibe deine files wie db.php, info.php, und das docker-compose in dein init file.
 4. ) schreibe im runcmd Teil was alles geschehen soll im container sobald dieser erstellt worden ist. 
 5. )  erstelle eine Ec 2 instanz und füge deine init Datei hinzu. wähle als key pair das von dem du die public key in deinem init gemacht hast. 
 6. ) Nachdem die Instanz lauft kannst du die öffentliche Adresse / deine Website nehmen um auf die gehosteten Website zuzugreifen 