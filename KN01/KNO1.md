# Installation
Screenshot der Website
![alt text](image.png)
Screenshot im Docker 
![alt text](image-1.png)
```
docker run -d -p 80:80 docker/getting-started   
// um einen Container zu benennen
docker run -d -p 80:80 --name yaya docker/getting-started   
// um den Container yaya zu nehmen
```
# B) Docker Command Line Interface (CLI)
1. Überprüfen Sie die Docker-Version. Welchen Befehl müssen Sie dafür verwenden?
```
docker --version
```
2. Suchen Sie nach dem offiziellen ubuntu und nginx Docker-Image auf Docker Hub mit dem
Befehl docker search .
````
docker search ubuntu
docker search nginx
```
3. In Teil A mussten Sie den Befehl docker run -d -p 80:80 docker/getting-started
ausführen. Erklären Sie die verschiedenen Parameter. 

docker run = ein Container wird gestartet
-d = es soll detached sein also im Hintergrund
-p 80:80 = der Port 80 von deinem localhost soll auf den Port 80 im Docker übertragen werden. 

4. Mit dem nginx Image verfahren Sie wie folgt. Wir zeigen, dass der Befehl docker run , das
gleiche ist wie die drei Befehle docker pull , docker create und docker start
hintereinander ausgeführt.
```
// Docker run = pull,create, start
docker pull nginx
docker create -p 8081:80 imagename
//verbindet port 8081 auf unserem Browser mit dem Port 80 in Docker und erstellt einen Container
docker start Containername
```
![alt text](<Screenshot 2024-05-17 154341.png>)
5. Mit dem ubuntu Image verfahren Sie wie folgt. Wir zeigen, dass nicht jedes Image im
Hintergrund ausgeführt werden kann.
```
docker run -d ubuntu
```
Beschreibung: ein Ubuntu kann nicht im Hintergrunf laufen wenn. Es muss immer eine Aktivität laufen sonst wird es beendet.
```
docker run -it ubuntu
```
Beschreibung: Hier wird eine interaktive Bash Shell erstellt die immer lauft. solange dieser Prozess lauft lauft auch der ubuntu Container.
6. Stellen Sie sicher, dass Ihr nginx-Container bereits läuft. Öffnen Sie nun nachträglich eine
interaktive Shell. Der Unterschied zu vorher ist, dass Sie nicht den Container mit interactiver
Shell starten, sondern eine Shell eines laufenden Containers öffnen. Der Befehl ist docker
exec -it <name-ihres-container> /bin/bash
![alt text](image-3.png)
7. Überprüfen Sie den Status der Container. Erstellen Sie einen Screenshot des Befehls und des Resultats. 
![alt text](image-4.png)
8.  Stoppen Sie nun noch den Container des nginx Images mit dem entsprechenden Docker-Befehl

```
docker stop containername
```
9.) docker rm containername
10.) docker rmi imagename

C) Registry und Repository (10%)
![alt text](<Screenshot 2024-05-18 140237.png>)

D)private Repository
```
docker tag nginx:latest ahmad851/ahmad851m347:nginx
```
das macht eine Kopie des Images und gibt es einen neuen Tag
```
docker push ahmad851/ahmad851m347:nginx
```
das pushed es auf Dockerhub hoch. 
![alt text](<Screenshot 2024-05-24 154354.png>)
das gleiche habe ich noch mit mariadb gemacht.