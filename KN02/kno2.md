# A Dockerfile 1.)
build image mit dem tag kn02a
```
docker build -t nginx:kn02a
```
![alt text](image.png)
create Container
![alt text](image-2.png)
```
docker create --name KN02 -p 80:80 ahmad851/ahmad851m347:kn02a
``` 
die website 
![alt text](image-3.png)
Container 
![alt text](image-4.png)
image 
![alt text](image-5.png)
Start des Containers
![alt text](image-6.png)
push des images ins docker hub private repo 
```
docker push ahmad851/ahmad851m347:kn02a
``` 
das ist wie docker push imagename:imagetag
Bild des Image was ich im repo gepostet habe
![alt text](image-8.png)
B. Dockerfile II
##  Docker Build Befehl
```
docker build -f dockerfile.mariadb -t ahmad851/ahmad851m347:kn02adb .
docker build -f dockerfile.web -t ahmad851/ahmad851m347:kn02aweb .
```
![alt text](image-15.png)
![alt text](image-16.png)
Es benutzt das Dockerfile dockerfile.mariadb um einen tag ahmad851/ahmad851m347:kn02adb zu erschaffen. Das Punkt am Schluss zeigt dass alles was man zum bilden braucht im jetzigen Verzeichnis ist.
## Docker Run Befehl
```
docker run -d -p 3306:3306 --name kn02adb ahmad851/ahmad851m347:kn02adb
docker run -d -p 80:80 --name kn02aweb --link kn02adb ahmad851/ahmad851m347:kn02web
```
![alt text](image-17.png)
![alt text](image-18.png)
sorgt das das image ahmad851/ahmad851m347 mit dem tag kn02adb einen Container bildet und dieser lauft.
## Verbindung zu Telnet
![alt text](image-11.png)
![alt text](image-10.png)
## info.php Seite
![alt text](image-12.png)
## db.php Seite
![alt text](image-13.png)

## push ins repo ![alt text](image-19.png)
![alt text](image-20.png)

# Alternative lösung mit compose
# Befehle
```
docker-compose build
```
sorgt dafür das docker compose builded.
```
docker-compose up -d
```
sorgt dafür das die beiden Container laufen
```
docker-compose down
```
baut alles was das compose gemacht hat wieder ab.

Push der db und web image ins repo
![alt text](image-14.png)

Zusammenfassung: 
Man muss ein Dockerfile für die beiden images machen und fügt diese files dann mit Hilfe eines docker compose zusammen. Danach kann man das docker Compose starten und abbauen mit den Befehlen oben. 