# wichtige Befehle und Erklärungen
```
 docker run -p hostport:containerport imagename 
```
 führt ein pull, create und start gleichzeitig aus. Beim erstellen des Containers verbindet es den hostport mit dem Containerport 
```
 docker create --name containername -p hostport: containerport imagename
 ```
 erschafft einen Container mit einem Namen und Ports. Der Container wird aus einem image gemacht. 

 ```
 Docker tag nginx:latest Benutzername/reponame:randomtag
 ```
Dieser Befehl erstellt einen neuen Tag für das image nginx. Jetzt verweisen die Tags nginx:latest und Benutzername/reponame:nginx beide auf das gleiche image.
```
docker push Benutzername/reponame:randomtag
```
das pushed das den Tag für das image auf Docker hub hoch. Jetzt habe ich eine kopie des images auf Dockerhub  
 # Docker File 
 ```
 FROM nginx 
WORKDIR /usr/share/nginx/html
COPY helloworld.html .
EXPOSE 80
# Es wird ein Image erstellt mit der
# Basis des Images nginx. Das heißt,
# wenn das Image bereits lokal vorhanden ist,
# wird es erweitert, und wenn nicht, wird es
# von Docker Hub gepulled.

# WORKDIR gibt das Arbeitsverzeichnis im
# Container an, in dem der nächste Befehl
# ausgeführt wird.

# COPY kopiert die Datei helloworld.html
# aus dem Verzeichnis, in dem sich die Dockerfile
# befindet (also das lokale Verzeichnis),
# in das aktuelle Arbeitsverzeichnis des Containers
# (das durch WORKDIR festgelegt wurde).

# EXPOSE gibt an, dass der Container den
# angegebenen Port (80) verwenden wird.
# Dies dient der Dokumentation und kann von
# Orchestrierungstools genutzt werden.
# Um den Port tatsächlich vom Host aus zugänglich
# zu machen, müssen Sie beim Starten des Containers
# einen Port-Mapping-Befehl verwenden, z.B. -p 8080:80.

 ```
 # Docker Compose
 ```
docker-compose build
```
sorgt dafür das docker compose builded.
```
docker-compose up -d
```
sorgt dafür das die beiden Container laufen
```
docker-compose down
```
baut alles was das compose gemacht hat wieder ab.
Zusammenfassung: 
Man muss ein Dockerfile für die beiden images machen und fügt diese files dann mit Hilfe eines docker compose zusammen. Danach kann man das docker Compose starten und abbauen mit den Befehlen oben. 


